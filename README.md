﻿**Summary**

This Script can be used to check login credentials of salesforce Org. We can also reset password of salesforce Org though this script.

**Prerequisite**

*Java 1.8 or above & Environment variable should be set for JDK.
*Eclipse marsh or higher version should be available in machine.
*Automation script execution required minimum i3 processer with 4GB Ram & 64 bit windows operating system.
*Microsoft office should be available for using excel file. 

**Setup**

*Open Eclipse.
*Click on File in eclipse menu option.
*Click on Import link.
*Select Git.
*import project from git with below Url & Credentials:-

Url:- 
https://navatarautomationteam@bitbucket.org/navatarautomationteam/checkorgstatus.git

User Name:- navatarip@navatargroup.com
Password:- automation@2018

       
**Configuration**

*Expand project in eclipse.
*Click on ‘credentials.properties’ file.
*Provide gmailUserName:- navatarindia101@gmail.com
*Provide gmailPassword:-navatar@321
*Provide EmailIDForStatusMail:- write email address where you want to get final report.
*Right click on Project & select Property option.
*Go to project location in local drive.
*Open ‘OrgStatusExcelSheet.xlsx’ file.
*Provide Username & Password of all salesforce Orgs.
*Provide updated password in case you want to change the password. If you want to check only Login status of org then it should be blank.
*Close ‘OrgStatusExcelSheet.xlsx’ file after saving credentials.
*Open ‘TestCases.xlsx’ file & make sure execute status should be ‘Yes’ in Module & TestCases sheet.
*Close ‘TestCases.xlsx’ file.
*Open browser & login in Gmail with user name :- navatarindia101@gmail.com & Password:- navatar@321 at least 1 time in new machine.


 

**Script Execution & Output**

*Go to Eclipse.
*Click on src folder.
*Expand launcher package
*Right click on executioner.java file
*Select Run as java application.
*Leave system idle until execution is not completed.
*After successfully completion you will get an email with status on configured email ID.
*Same status of orgs can be check in ‘OrgStatusExcelSheet.xlsx’ file in local system.


**Limitations**

*Make sure network IP of execution machine should be white listed in salesforce org.
*Make sure ‘OrgStatusExcelSheet.xlsx’ & ‘TestCases.xlsx’ file should be close before starting the execution.
*Please don’t stop execution in middle, if you terminate the execution in middle, the status will not be written on ‘OrgStatusExcelSheet.xlsx’ file.



