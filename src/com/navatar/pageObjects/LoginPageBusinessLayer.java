/**
 * 
 */
package com.navatar.pageObjects;

import static com.navatar.generic.CommonLib.*;
import static com.navatar.generic.CommonVariables.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.navatar.generic.EnumConstants.action;
import com.navatar.generic.EnumConstants.excelLabel;
import com.navatar.generic.ExcelUtils;


public class LoginPageBusinessLayer extends LoginPage implements LoginErrorPage {

	
	/**
	 * @param driver
	 */
	public LoginPageBusinessLayer(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	

	public boolean checkOrgStatus(String userName, String password) {
		BasePageBusinessLayer bp=new BasePageBusinessLayer(driver);
		String Loginstatus="",updatedpassword="";
		Boolean flag = false;
		driver.get("https://"+URL);
		if(sendKeys(driver, getUserNameTextBox(20), userName, "Username Text Box", action.SCROLLANDBOOLEAN)) {
			appLog.info("Passed value in userName Text Box : "+userName);
			if(sendKeys(driver, getPasswordTextBox(20), password, "Password Text Box", action.THROWEXCEPTION)) {
				appLog.info("Passed value in password Text Box : "+password);
				if(click(driver, getLoginButton(20), "Login Button", action.THROWEXCEPTION)) {
					appLog.info("clicked On LoginButton");
					waitForPageLoad(driver);
					if (matchTitle(driver, "Home", 2) || matchTitle(driver, "Salesforce - Enterprise Edition", 2) || bp.getSalesForceLightingIcon(3) != null || getUserMenuTab(3) != null) {
						appLog.info("Successfully logged in for user "+userName);
						updatedpassword=ExcelUtils.readData(ExcelUtils.OrgStatusExcelSheet,"OrgCredential",excelLabel.UserName,userName, excelLabel.UpdatePassword);
						if(!updatedpassword.isEmpty() && updatedpassword!=null) {
							if(updatePassword(userName, password, updatedpassword)) {
								Loginstatus="Successfully logged in and Update Password.";
								loggedIn++;
								flag= true;
							}else {
								Loginstatus="Successfully logged in but could not update password.";
								loggedIn++;
								flag= true;
							}
						}else {
							Loginstatus="Successfully logged in.";
							loggedIn++;
							flag= true;
						}
					}else if (matchTitle(driver, "Trial Expired | Salesforce", 1)) {
						appLog.info("Org Trial is expired for user : "+userName);
						Loginstatus="Org Trial is Expired";
						ExpiredOrg++;
						flag= true;
					}else if (matchTitle(driver, "Change Your Password | Salesforce", 1)) {
						appLog.info("Required Change Password for user : "+userName);
						Loginstatus="Required Change Password.";
						ChangePassword++;
						flag= true;
					}else if (matchTitle(driver, "Verify Your Identity | Salesforce", 1)) {
						appLog.info("Required Verification Code for  this user Login : "+userName);
						Loginstatus="Required Verification Code.";
						RequiredCode++;
						flag= true;
					}else {
						appLog.info("Invalid Credentials for user "+userName);
						Loginstatus="Invalid Credentials.";
						InvalidCredential++;
						flag= true;
					}
					ExcelUtils.writeData(ExcelUtils.OrgStatusExcelSheet,Loginstatus,"OrgCredential", excelLabel.UserName,userName,
							excelLabel.OrgStatus);
				}else {
					appLog.error("Not able to click on Login button for user : "+userName);
				}
				
			}else {
				appLog.error("Not able to pass value in password text box : "+password+" for user : "+userName);
			}
			
		}else {
			appLog.error("Not able to pass value in user Name text box for user : "+userName);
		}
		return flag;
	}
	
	
	public boolean updatePassword(String userName, String password,String updatedpassword) {
		boolean flag = false;
		WebElement usermenuTab=null;
		usermenuTab=getUserMenuTab(2);
		if(usermenuTab!=null) {
			click(driver, getLightingCloseButton(10), "Lighting Pop-Up Close Button.", action.BOOLEAN);
			if(click(driver, getUserMenuTab(10), "user menu tab", action.SCROLLANDBOOLEAN)) {
				appLog.info("clicked on user menu tab ");
				if(click(driver, getSetUpLink(10), "setup link", action.SCROLLANDBOOLEAN)) {
					appLog.info("clicked on setup link");
					ThreadSleep(3000);
					if(click(driver, getMyPersonalInformationLink(10), "my personal information link", action.SCROLLANDBOOLEAN)) {
						appLog.info("clicked on my personal information link in classic");
						if(click(driver, getChangeMyPasswordLinkClassic(10), "change my password", action.SCROLLANDBOOLEAN)) {
							appLog.info("clicked on changed my password link in classic");
							flag=true;
						}else {
							appLog.error("Not able to change my password link in classic so cannot update user : "+userName+" Password");
						}
					}else {
						appLog.error("Not able to expand my personal information link in classic so cannot update user : "+userName+" Password");
					}
				}else {
					appLog.error("Not able to click on Setup link in classic so cannot update user :  "+userName+" Password.");
				}
			}else {
				appLog.error("Not able to click on user menu tab in classic so cannot update user : "+userName+" Password.");
			}
		}else {
			if(click(driver, getSalesForceLightingIcon(30), "login icon", action.BOOLEAN)) {
				appLog.info("clicked on Login Icon");
				if(click(driver, getSettings_icon(30), "settings link", action.SCROLLANDBOOLEAN)) {
					appLog.info("clicked on settings link");
					ThreadSleep(3000);
					if(click(driver, getChangePasswordLink(30), "change password link", action.SCROLLANDBOOLEAN)) {
						appLog.info("clicked on change password link ");
						if(switchToFrame(driver, 60, getChangePasswordIframe(60))) {
							appLog.info("switch in change password frame ");
							flag=true;
						}else {
							appLog.error("Not able to click switch on change password Iframe : "+userName+" so cannot update password");
						}
					}else {
						appLog.error("Not able to click on change password link so cannot update password "+userName);
					}
				}else {
					appLog.error("Not able to click on settings link so cannot update password "+userName);
				}
			}else {
				appLog.error("Not able to click on login icon so cannot update password "+userName);
			}
		}
		if(flag) {
			if(sendKeys(driver, getCurrentPassword(30), password, "current password text box ", action.SCROLLANDBOOLEAN)) {
				appLog.info("Enter current value : "+password);
				if(sendKeys(driver, getChangePasswordNewPassword(30), updatedpassword, "new password text box ", action.SCROLLANDBOOLEAN)) {
					appLog.info("Enter new password value : "+updatedpassword);
					if(sendKeys(driver, getChangepasswordconfirmPassword(30), updatedpassword, "new password text box ", action.SCROLLANDBOOLEAN)) {
						appLog.info("Enter confirm new password  value : "+updatedpassword);
						if(click(driver, getChangePasswordSaveButton(30), "save button", action.SCROLLANDBOOLEAN)) {
							appLog.info("clicked on save button");
							ThreadSleep(3000);
							if(usermenuTab!=null) {
								if(getPEAdminHeaderAfterChangePassword(5)!=null) {
									appLog.info("User "+userName+" Password has been Updated");
									return true;
								}else {
									appLog.error("Password Change Error Message is not visible so cannot verify Update Password Message "+userName);
								}
							}else {
								if(getChangePasswordSuccessfulMsg(5)!=null) {
									String msg = getChangePasswordSuccessfulMsg(30).getText().trim();
									appLog.info("verify Password Change Message : "+msg);
									return true;
								}else {
									appLog.error("Password Change Error Message is not visible so cannot verify Update Password Message "+userName);
								}
							}
						}else {
							appLog.error("Not able to click on update password save button  : "+userName+" so cannot update password");
						}
					}else {
						appLog.error("Not able to pass enter confirm new  password  : "+userName+" so cannot update password");
					}
				}else {
					appLog.error("Not able to pass enter new password  : "+userName+" so cannot update password");
				}
			}else {
				appLog.error("Not able to pass enter current password  : "+userName+" so cannot update password");
			}
		}
		return false;
	}
	
	
	
}
	
	
