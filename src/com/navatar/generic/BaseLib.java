package com.navatar.generic;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import com.relevantcodes.extentreports.ExtentReports;



public class BaseLib extends AppListeners {

	public static WebDriver edriver;
	public static EventFiringWebDriver driver;
	public AppListeners testListner;
	public static SoftAssert sa=new SoftAssert();
	public CommonVariables cv = null;//common variable reference
	public static String downloadedFilePath=System.getProperty("user.dir")+"\\DownloadedFiles";
	public static String testCasesFilePath = System.getProperty("user.dir")+"/TestCases.xlsx";
	public static WiniumDriver dDriver = null;
	public static List<String> ListofUploadedfiles = new ArrayList<String>();
	public static int loggedIn=0;
	public static int ExpiredOrg=0;
	public static int ChangePassword=0;
	public static int InvalidCredential=0;
	public static int RequiredCode=0;
	
	
	@BeforeSuite
	public void reportConfig(){
		DateFormat dateFormat = new SimpleDateFormat("yy_MM_dd_hh_mm_ss");
		Date date = new Date();
		extentReport = new ExtentReports(
				System.getProperty("user.dir") + "/Reports/ExtentReports/ExtentLog" + dateFormat.format(date) + ".html",
				true);
		try {
			extentReport.addSystemInfo("Host Name", InetAddress.getLocalHost().getHostName());
			extentReport.addSystemInfo("User Name", System.getProperty("user.name"));
			extentReport.addSystemInfo("Environment", ExcelUtils.readDataFromPropertyFile("Environment"));
			extentReport.addSystemInfo("Mode", ExcelUtils.readDataFromPropertyFile("Mode"));
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		extentReport.loadConfig(new File(System.getProperty("user.dir") + "\\ConfigFiles\\extent-config.xml"));
	}
	
	@Parameters(value = "browser")
	@BeforeMethod
	public void config(String browserName){
		if (browserName.equalsIgnoreCase("Chrome")) {
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + "\\exefiles\\chromedriver.exe");
			
//			WebDriverManager.chromedriver().setup();
			ChromeOptions options = new ChromeOptions();
			
			
			options.setBinary("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
			options.addArguments("disable-infobars");
			options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
//			options.setExperimentalOption("excludeSwitches", Arrays.asList("disable-popup-blocking"));
			options.setExperimentalOption("useAutomationExtension", false);
			options.addArguments("start-maximized");
			
			System.setProperty(ChromeDriverService.CHROME_DRIVER_SILENT_OUTPUT_PROPERTY, "true");
		    Logger.getLogger("org.openqa.selenium").setLevel(Level.OFF);
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.default_content_settings.popups", 0);
			prefs.put("download.default_directory", downloadedFilePath);
			prefs.put("profile.password_manager_enabled", false);
			options.setExperimentalOption("prefs", prefs);
			options.addArguments("--disable-notifications");
//			DesiredCapabilities dp = new DesiredCapabilities();
//			dp.setCapability(ChromeOptions.CAPABILITY, options);
//			dp.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			
			edriver = new ChromeDriver(options);
			
			
		} else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver",
					System.getProperty("user.dir") + "\\exefiles\\geckodriver.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			FirefoxOptions options = new FirefoxOptions();
			options.addPreference("log", "{level: trace}");
			capabilities.setCapability("marionette", true);
			capabilities.setCapability("moz:firefoxOptions", options);
			edriver = new FirefoxDriver(options);
		} else if (browserName.equalsIgnoreCase("IE Edge")) {
			 System.setProperty("webdriver.edge.driver", System.getProperty("user.dir") +"\\exefiles\\MicrosoftWebDriver.exe");
			 edriver = new EdgeDriver();
		}else {
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + "\\exefiles\\chromedriver.exe");
			edriver = new ChromeDriver();
		}
		testListner = new AppListeners();
		driver = new EventFiringWebDriver(edriver);
		driver.register(testListner);
		cv=new CommonVariables(this);
	}
	
	

	@AfterMethod
	public void settingsAfterTests(ITestResult result) {
		driver.close();
		try {
			Process process = Runtime.getRuntime().exec(System.getProperty("user.dir")+"/killbrowser.bat");
			process.waitFor();
			CommonLib.ThreadSleep(1000);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sa=new SoftAssert();
		String toValue = ExcelUtils.readDataFromPropertyFile("EmailIdForStatusMail");
		String[] attachment = {"OrgStatusExcelSheet.xlsx"};
		String from = ExcelUtils.readDataFromPropertyFile("gmailUserName");
		String Password = ExcelUtils.readDataFromPropertyFile("gmailPassword");
//		String userName = System.getProperty("user.name");
		String[] to = {from};
		if(!toValue.isEmpty()){
			to = new String[toValue.split(",").length];
			for(int i = 0; i < toValue.split(",").length; i++){
				to[i]=toValue.split(",")[i];
				if(!toValue.isEmpty()) {
					try {
						EmailLib.sendMail(from, Password, to,"Org Login Status", "Dear User,"+"<br><br>"+"Please check the Login Summery of the provided Orgs.", attachment);
					} catch (MessagingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} 
	}
	
//	@AfterSuite
	public void closeBrowser(){
		driver.close();
		try {
			Process process = Runtime.getRuntime().exec(System.getProperty("user.dir")+"/killbrowser.bat");
			process.waitFor();
			CommonLib.ThreadSleep(1000);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

}