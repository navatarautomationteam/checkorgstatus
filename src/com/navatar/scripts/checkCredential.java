package com.navatar.scripts;


import org.testng.annotations.Test;
import com.navatar.generic.BaseLib;
import com.navatar.generic.ExcelUtils;
import com.navatar.pageObjects.LoginPageBusinessLayer;
import static com.navatar.generic.CommonVariables.*;
import static com.navatar.generic.ExcelUtils.*;
public class checkCredential extends BaseLib {
	String passwordResetLink = null;
	
	
	
	@Test
	public void Tc001_CheckOrgCredentials() {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		String[] userName=readAllDataForAColumn(ExcelUtils.OrgStatusExcelSheet, "OrgCredential", 0, false).split(",");
		String[] password=readAllDataForAColumn(ExcelUtils.OrgStatusExcelSheet, "OrgCredential", 1, false).split(",");
		for(int i=0; i<userName.length; i++) {
			if(lp.checkOrgStatus(userName[i], password[i])) {
				appLog.info("Org Status is checked for user : "+userName[i]);
							
			}else {
				appLog.error("Not able to check Org Status for User : "+userName[i]);
				sa.assertTrue(false, "Not able to check Org Status for User : "+userName[i]);
			}
			if(i!=userName.length-1) {
				driver.close();
				config(browserToLaunch);
				 lp = new LoginPageBusinessLayer(driver);
			}
		}
		sa.assertAll();
	}
	
	
	
	
	

	
	

}
